<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Project Manager</title>

	<link rel="stylesheet" href="<spring:url value="/resources/css/bootstrap.min.css"/>" type ="text/css">
	<link rel="stylesheet" href="<spring:url value="/resources/css/home.css"/>" type="text/css"/>
	<link rel="stylesheet" href="<spring:url value="/resources/css/bootstrap-select.min.css"/>" type ="text/css">
	<script src="<spring:url value="/resources/js/jquery-1.11.2.min.js"/>" ></script>
	<script src="<spring:url value="/resources/js/bootstrap.min.js"/>" ></script>
	<script src="<spring:url value="/resources/js/bootstrap-select.min.js"/>" ></script>
</head>
<body>
	
	<jsp:include page="../views/fragments/header.jsp"></jsp:include>			

	<div class="container">
		<h2>Current Project</h2>
		<ul class="">
  			<li class=""><label>Project Name:</label><span>${currentProject.name }</span></li>
  			<li class=""><label>Sponsor:</label><span>${currentProject.sponsor }</span></li>
  			<li class=""><label>Description:</label><br/><span>${currentProject.description }</span></li>
		</ul>
	</div>
</body>
</html>