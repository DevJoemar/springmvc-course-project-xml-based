package com.devjoemar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {
		
		@RequestMapping("/home")
		public String goHome(Model model) {
//			Project project = new Project();
//			project.setName("NEW SPRING PROJECt");
//			project.setSponsor("NASA");
//			project.setDescription("A new project");
//			
//			model.addAttribute("currentProject", project);
			return "home";
		}
}